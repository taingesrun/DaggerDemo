package com.example.tainge.daggerdemo;

/**
 * Created by Tainge on 11/15/2017.
 */

public class Student {
    String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.example.tainge.daggerdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.tainge.daggerdemo.component.DaggerStudentComponent;
import com.example.tainge.daggerdemo.component.StudentComponent;
import com.example.tainge.daggerdemo.module.ContextModule;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadStudcent();
    }


    public void loadStudcent(){
        StudentComponent component = DaggerStudentComponent
                .builder()
                .contextModule(new ContextModule(this))
                .build();

        Student student = component.getStudent();

        Log.e("student",student.toString());

    }
}

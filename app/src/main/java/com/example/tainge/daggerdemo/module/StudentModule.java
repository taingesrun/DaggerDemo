package com.example.tainge.daggerdemo.module;

import com.example.tainge.daggerdemo.Student;
import com.example.tainge.daggerdemo.component.ActivityScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;


@Module
public class StudentModule {


    @Provides
    @ActivityScope
    public Student student(@Named("name") String name){
        return new Student(name);
    }

    @Provides
    @ActivityScope
    @Named("name")
    public String name(){
        return "Hello My World";
    }

    @Provides
    @ActivityScope
    @Named("other")
    public String otherName(){
        return "Other Name";
    }
}

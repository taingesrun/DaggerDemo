package com.example.tainge.daggerdemo.component;

import android.content.Context;

import com.example.tainge.daggerdemo.Student;
import com.example.tainge.daggerdemo.module.ContextModule;
import com.example.tainge.daggerdemo.module.StudentModule;

import dagger.Component;


@Component(modules = {StudentModule.class, ContextModule.class})
@ActivityScope
public interface StudentComponent {
    public Context getContext();
    public Student getStudent();
}

package com.example.tainge.daggerdemo.module;

import android.content.Context;

import com.example.tainge.daggerdemo.component.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
   private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @ActivityScope
    public Context getContext(){
        return context;
    }
}
